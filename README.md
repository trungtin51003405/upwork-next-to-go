# Next to go demo app

The following demo app runs a server to serve some race data. The front end uses
react and redux to manage application state and handle the rendering of the view.
The server handles the initial render server side.

Webpack is used to handle the build compilation for both the server and browser.

## How to

To start the app run the following in order:

```
npm install
```

To start the server run:

```
npm run dev
```

You can access the app at localhost:8080 on your machine.


To build:

```
npm run build
```
You can the app by index.html.