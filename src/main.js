import Vue from 'vue';
import VueResource from 'vue-resource';
Vue.use(VueResource);
// Vue.http.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8080';
// Vue.http.headers.common['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS';
// Vue.http.headers.common['Access-Control-Request-Headers'] = 'Content-Type';
// Vue.http.headers.common['Origin'] = 'https://api.beta.tab.com.au';
// Vue.http.headers.common['Access-Control-Allow-Credentials'] = 'true';

Vue.component('next-to-go', require('./components/NextToGo.vue'));

const app = new Vue({
    el: '#app',
});

